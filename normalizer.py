#! /usr/bin/env python3
# Danial Behzadi - 2018
# Released under GPLv3+

def normalize(text):
    text.replace("ك", "ک")
    text.replace("ي", "ی")
    text.replace("ئ", "ی")
    text.replace("‌", " ")
    return text
